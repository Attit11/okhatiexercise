import React from 'react';
import './App.css';
import LogIn from './login';

function App() {
  return (
    <div className="App">
      <LogIn />
    </div>
  );
}

export default App;
